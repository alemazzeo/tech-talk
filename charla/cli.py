import pathlib

import click

import charla.text_operations as to


@click.group()
def cli():
    pass


@cli.command()
@click.argument(
    "file",
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        readable=True,
        path_type=pathlib.Path,
    ),
)
def count_words(file: pathlib.Path):
    with open(file, mode="r") as f:
        text = f.read()

    result = to.count_words(text)
    for word, count in sorted(result.items(), key=lambda x: x[1], reverse=True):
        print(f"{word:20s}: {count:02d}")


if __name__ == "__main__":
    cli()
