import charla.text_operations as to


def test_count_words():
    result = to.count_words("uno   uno   dos tres")
    assert result == {"uno": 2, "dos": 1, "tres": 1}
