# Roadmap

- [x] Crear un proyecto con Poetry
- [x] Configurar herramientas
    - [x] .gitignore
    - [x] .dockerignore
    - [x] pyproject.toml
    - [x] .pre-commit-config.yaml
- [x] Agregar un script de ejemplo
- [x] Crear puntos de entrada para la aplicación
- [x] Crear el archivo Dockerfile
- [x] Probar la ejecución en modo local
- [ ] Definir el pipeline de CI/CD
- [ ] Probar la ejecución en la Nube